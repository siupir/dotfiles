# vi: filetype=zsh

# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=100000
SAVEHIST=100000
setopt autocd extendedglob nomatch notify
unsetopt beep
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/Users/irmis/.zshrc'
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Za-z}'

autoload -Uz compinit
compinit
# End of lines added by compinstall

source "$HOME/.config/zsh/general.zsh"
source "$HOME/.config/zsh/aliases.zsh"
source "$HOME/.config/zsh/plugins.zsh"
source "$HOME/.config/zsh/prompt.zsh"
