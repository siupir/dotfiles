#!/bin/bash

res=$(echo "Logout|Reboot|Shutdown" | rofi -sep "|" -dmenu -i -p 'Power Menu' -bw 1 -bc "#E76642" -bg "#202529" -fg "#E76642" -hlbg "#32393D" -hlfg "#dc1900" -width 15 -eh 4 -location 3 -yoffset 22 -padding 3 -font "Iosevka 11")

#if [ $res = "lock" ]; then
#    /home/khoaduccao/.config/lock.sh
#fi
if [[ $res == "Logout" ]]; then
   i3-msg exit
fi
if [[ $res == "Reboot" ]]; then
  systemctl reboot
fi
if [[ $res == "Shutdown" ]]; then
    systemctl poweroff
fi
exit 0
