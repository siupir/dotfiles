#!/bin/bash

SESSION=dev

IS_SESSION=$(tmux list-sessions | grep $SESSION)
echo $IS_SESSION
if [ "$IS_SESSION" = "" ]
then
    # creates new session
    tmux new-session -d -s $SESSION

    # just renames first window
    tmux rename-window -t $SESSION:1 'main'

    # create new window
    tmux new-window -t $SESSION:2 -n 'alt'
fi

tmux attach-session -t $SESSION:1
