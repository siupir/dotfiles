call plug#begin('~/.vim/plugged')

" Auto pairs for '(' '[' '{'
Plug 'jiangmiao/auto-pairs'

" rainbow brackets
Plug 'luochen1990/rainbow'
let g:rainbow_active = 1

" Airline (bottom status)
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" Shows git status on line numbers
Plug 'airblade/vim-gitgutter'

" Telescope file finder
Plug 'nvim-lua/popup.nvim'
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'

" LSP
Plug 'neovim/nvim-lspconfig'

Plug 'nelsyeung/twig.vim'

Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plug 'code-biscuits/nvim-biscuits'

" autocomplete
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/cmp-path'
Plug 'hrsh7th/cmp-cmdline'
Plug 'hrsh7th/nvim-cmp'

" snippets
Plug 'hrsh7th/cmp-vsnip'
Plug 'hrsh7th/vim-vsnip'

Plug 'norcalli/nvim-colorizer.lua'

" themes
Plug 'joshdick/onedark.vim'

Plug 'whatyouhide/vim-gotham'

Plug 'rakr/vim-one'

Plug 'kyazdani42/nvim-web-devicons'
Plug 'romgrk/barbar.nvim'

Plug 'tpope/vim-sleuth'

Plug 'kristijanhusak/orgmode.nvim'

Plug 'github/copilot.vim'

call plug#end()
