# vi: filetype=zsh

# editor config from (https://github.com/posquit0/zshrc/blob/main/general.zshrc)
if [[ -n $SSH_CONNECTION ]]; then
  export EDITOR="$( echo $(which nvim) || echo $(which vim) || echo $(which vi) )"
else
  export EDITOR="$( echo $(which nvim) || echo $(which vim) || echo $(which vi) )"
fi

ZSH_HOST_FILE="$HOME/.config/zsh_host.zsh"

if [ -f "$ZSH_HOST_FILE" ]; then
  source "$ZSH_HOST_FILE"
fi

export LANG=en_US.UTF-8
export LC_CTYPE=en_US.UTF-8

export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
