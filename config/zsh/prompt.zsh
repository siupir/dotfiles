# vi: filetype=zsh

autoload -U colors && colors
#PS1="%B%{$fg[red]%}[%{$fg[yellow]%}%n%{$fg[green]%}@%{$fg[blue]%}%M %{$fg[magenta]%}%~%{$fg[red]%}]%{$reset_color%}$%b "
#PS1="%F{blue}%~ ${GITSTATUS_PROMPT}%F{reset}➜ "

PROMPT='%F{blue}%~%f $GITSTATUS_PROMPT '  # right prompt: git status
#PROMPT+=$'\U279C %# '
PROMPT+=$'\U279C '
#PROMPT+='› ';
#PROMPT+='➜'               # left prompt: directory followed by %/# (normal/root)

