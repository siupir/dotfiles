# vi: filetype=zsh

# zsh-autocomplete plugin and config
zstyle ':autocomplete:*' min-input 1
zstyle ':autocomplete:*' min-delay 0.0

# zsh vim mode indicator ( commented because has issues with zsh-syntax-highlighter)
#source "$HOME/.config/zsh/plugins/vi-mode/vi-mode.zsh"

#source "$HOME/.config/zsh/plugins/zsh-autocomplete/zsh-autocomplete.plugin.zsh"

# zsh git status plugin
source "$HOME/.config/zsh/plugins/gitstatus/gitstatus.prompt.zsh"

# fish like syntach highlighting
source "$HOME/.config/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"
