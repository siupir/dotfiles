# vi: filetype=zsh

alias lrc="$EDITOR $HOME/.config/zsh_host.zsh"
alias rc="$EDITOR $HOME/.zshrc"
alias gerc="$EDITOR $HOME/.config/zsh/general.zsh"
alias alrc="$EDITOR $HOME/.config/zsh/aliases.zsh"
alias plrc="$EDITOR $HOME/.config/zsh/plugins.zsh"
alias prrc="$EDITOR $HOME/.config/zsh/prompt.zsh"
alias scrc="source $HOME/.zshrc"
alias scalrc="source $HOME/.config/zsh/aliases.zsh"
alias vv="$EDITOR $HOME/.config/nvim/general/settings.vim"
alias vp="$EDITOR $HOME/.config/nvim/plugins.vim"
alias vvi="$EDITOR $HOME/.config/nvim/init.vim"

alias ls="$( echo $(which exa) || echo $(which ls) )"
alias ll="ls -la"
alias lg="exa -a --long --git --group-directories-first --icons"
alias l="ls -l"
alias l.="ls -ld .*"
alias v="$EDITOR"
alias cl="clear"
alias cn="clear && neofetch"

alias gp="git push"
alias gts="git status"

alias dcu="docker-compose up"
alias dcb="docker-compose up --build"
alias dcd="docker-compose up -d"
alias dcr="docker-compose run --rm"

alias t="tmux -u"
alias tmux="tmux -u"
